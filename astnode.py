import enum

class NodeType(enum.Enum):
    empty = enum.auto()
    
    # 
    document = enum.auto()
    subfile = enum.auto()
    frame = enum.auto()
    
    # block elements
    list = enum.auto()
    ordered = enum.auto()
    table = enum.auto()
    comment = enum.auto()
    quote = enum.auto()
    blockcode = enum.auto()
    blockmath = enum.auto()
    
    # inline elements
    link = enum.auto()
    img = enum.auto()
    bold = enum.auto()
    italic = enum.auto()
    incode = enum.auto()
    inmath = enum.auto()
    row = enum.auto()
    text = enum.auto()
    heading = enum.auto()
    listitem = enum.auto()
    ordereditem = enum.auto()

    # animation control
    pause = enum.auto()

astEndPos = (NodeType.link, NodeType.img, NodeType.incode, NodeType.inmath, NodeType.text)
astLinePos = (NodeType.blockmath, NodeType.blockmath, NodeType.heading, NodeType.table, NodeType.list, NodeType.ordered)

class Stat: pass
class AstNode:
    def __new__(cls, *args, **kwargs):
        obj = super().__new__(cls)
        obj.kids, obj.latex = [], ""
        obj.latex_begin, obj.latex_end = "", ""
        obj.type, obj.text = NodeType.empty, ""
        return obj
    
    def __init__(self, nodetype=NodeType.empty, master=None, *args, **kwargs):
        self.conf = kwargs
        self.parent = master
        self.type = nodetype
        
    def output(self, lev = 0):
        def prt(*args, **kwargs):
            print("\t" * lev, *args, **kwargs)
        prt("Type:", self.type)
        prt("Conf:", self.conf)
        prt("\tText:", self.text)
        for kid in self.kids:
            kid.output(lev + 1)

