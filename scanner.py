from pathlib import Path
from astnode import *

class Scanner:
    def __init__(self, basedir, entry=True, enc="utf-8"):
        self.basedir = Path(basedir)
        self.enc = enc
        self.root = AstNode(NodeType.document if entry else NodeType.subfile)
        
    def parseFile(self, filename):
        with open(self.basedir / filename, encoding=self.enc) as f:
            doctext = f.read()
        self.root.conf["file"] = filename
        return self.parseText(doctext)
        
    def parseText(self, doctext):
        stat = Stat()
        
        stat.inhead, stat.subignore = False, False
        conf, text = [], []
        frame = None
        
        for line in doctext.split('\n'):
            if line.startswith("---"): # head end or begin
                if not stat.inhead: # new frame start
                    if frame is not None and not stat.subignore:
                        frame.conf["text"] = text
                        # frame.text = text
                        self.root.kids.append(frame)
                    
                    stat.subignore = False
                    conf, text = [], []
                    frame = AstNode(NodeType.frame, self.root)
                stat.inhead = not stat.inhead
                continue
            
            if line.strip() == "" and stat.inhead: # line empty, heading end
                stat.inhead = False
                continue
            
            if stat.inhead:
                key, val = line.split(':', 1)
                if key == "src":
                    filename = Path(val.strip())
                    scan = Scanner(self.basedir, False, self.enc)
                    subfile = scan.parseFile(filename)
                    subfile.parent = self.root
                    self.root.kids.append(subfile)
                else:
                    frame.conf[key] = val.strip()
            else:
                text.append(line)

        if frame is not None:
            frame.conf["text"] = text
            self.root.kids.append(frame)
        
        return self.root
