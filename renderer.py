from pathlib import Path

from jinja2 import FileSystemLoader, Environment, select_autoescape

from astnode import *

teplenv = Environment(
        loader=FileSystemLoader("templates"),
        autoescape=select_autoescape()
)

tepls = dict(
    head = teplenv.get_template("head.tex"),
)

docbegin = "\\begin{document}"
docend = "\\end{document}"

def md2text(file):
    p = Path(file)
    return p.parent / (p.stem + ".tex")

class Renderer:
    def __init__(self, basedir = Path.cwd() / "dist", docConf = {}, enc="utf-8"):
        basedir.mkdir(exist_ok=True)
        self.basedir = basedir
        self.docConf = docConf
        self.enc = enc
        self.pause = 0
        self.fp = None

    def render(self, node: AstNode):
        if node.type == NodeType.document:
            node.conf["target"] = md2text(node.conf["file"])
            self.entry = node.conf["target"]
            node.latex = tepls["head"].render(**node.conf, **self.docConf) + "\n{%s}\n" % docbegin
            node.latex_end = docend
        elif node.type == NodeType.subfile:
            node.conf["target"] = md2text(node.conf["file"])
            node.latex_begin = "\\subfile{%s}\n" % node.conf["target"]
            node.latex = "\\documentclass[%s]{subfiles}\n{%s}\n" % (self.entry, docbegin)
            node.latex_end = docend
        elif node.type == NodeType.frame:
            node.latex_begin = "\\begin{frame}[fragile]"
            node.latex_end = "\\end{frame}"
        elif node.type == NodeType.list:
            node.latex_begin = "\\begin{itemize}"
            node.latex_end = "\\end{itemize}"
        elif node.type == NodeType.ordered:
            node.latex_begin = "\\begin{enumerate}"
            node.latex_end = "\\end{enumerate}"
        elif node.type == NodeType.listitem:
            node.latex_begin = "\\item"
        elif node.type == NodeType.ordereditem:
            node.latex_begin = "\\item[%s]" % node.conf["item-index"]
        elif node.type == NodeType.blockcode:
            node.latex_begin = "\\begin{verbatim}"
            node.latex_end = "\\end{verbatim}"
        elif node.type == NodeType.blockmath:
            node.latex_begin = "\\["
            node.latex_end = "\\]"
        elif node.type == NodeType.quote:
            node.latex_begin = "\\fcolorbox{black}{gray!10}{\parbox{.9\\linewidth}{"
            node.latex_end = "}}"
        elif node.type == NodeType.table:
            node.latex_begin = "\\begin{table}[h!]\n\\begin{center}\\begin{tabular}"
            node.latex_end = "\\end{tabular}\n\\end{center}\n\\end{table}"
        elif node.type == NodeType.row:
            node.latex_end = " & "
        elif node.type == NodeType.bold:
            node.latex_begin = "\\textbf{"
            node.latex_end = "}"
        elif node.type == NodeType.italic:
            node.latex_begin = "\\textit{"
            node.latex_end = "}"
        elif node.type == NodeType.incode:
            node.latex_begin = "\\texttt{"
            node.latex_end = "}"
        elif node.type == NodeType.inmath:
            node.latex_begin = "$"
            node.latex_end = "$"
        elif node.type == NodeType.link:
            node.text = "\\href{%s}{%s}" % (node.conf["src"], node.conf["alt"])
        elif node.type == NodeType.img:
            node.text = "\\begin{figure}\n\\centering\n\\includegraphics[width=\\textwidth]{%s}\n%s\n\\end{figure}" % (
                node.conf["src"], "\label{%s}" % node.conf["alt"] if node.conf["alt"] else ""
            )

        if node.type in astEndPos:
            node.latex = node.text
       
        use = 1 if node.type in astEndPos and node.latex else 0
        for kid in node.kids:
           use += self.render(kid)
        
        if use == 0:
            node.latex, node.latex_begin, node.latex_end = "", "", ""
        return use

    def output(self, root):
        def prt(*args, **kwargs):
            if len(args) > 0 and args[0]:
                print(*args, **kwargs, file = self.fp)

        if root.type == NodeType.document or root.type == NodeType.subfile:
            oldf = self.fp
            if root.latex_begin:
                prt(root.latex_begin)
            with open(self.basedir / root.conf["target"], "w", encoding=self.enc) as f:
                self.fp = f

                prt(root.latex)
                for kid in root.kids:
                    self.output(kid)
                prt(root.latex_end)
            self.fp = oldf
        else:
            if root.type == NodeType.pause:
                self.pause += root.conf["pause-type"]
                if not self.lastpause:
                    prt("\\pause")
                self.lastpause = True
            elif root.type in astLinePos:
                if self.pause == 2 and not self.lastpause:
                    prt("\\pause")
                self.lastpause = True
                
            if root.latex or root.latex_begin or root.latex_end:
                self.lastpause = False

            prt(root.latex_begin)
            prt(root.latex)
            for kid in root.kids:
                self.output(kid)
            prt(root.latex_end)
