import re
from constants import *
from astnode import *
from pathlib import Path

matdict = dict(
    list = re.compile(r"- "),
    ordered = re.compile(r"\d+\. "),
    link = re.compile(r"\[(.*?)\]\((.*?)\)"),
)

class Tokenizer:
    def __init__(self):
        pass

    def analyze(self, root):
        if root.type == NodeType.frame:
            self.analyze_frame(root)
        else:
            for kid in root.kids:
                # kid.parent = root
                self.analyze(kid)

    def analyze_frame(self, frame):
        stat = Stat()
        stat.lev, stat.lastspace = 0, [-1]
        stat.code, stat.math = False, False
        stat.table = False

        def puttext(node, line):
            kid = astkid(kidtype.text, node)
            node.kids.append(kid)
            kid.text = line

        maxlev = 0
        # BUG: 无法处理列表内层级元素的情况
        # TODO: 没有处理 table 的情况

        for line in frame.conf["text"]:
            if line.startswith("$$") or line.startswith("```"):
                if stat.math and line.startswith("$$"):
                    frame = frame.parent
                    stat.math = False
                elif stat.code and line.startswith("```"):
                    frame = frame.parent
                    stat.code = False
                elif stat.math or stat.code:
                    puttext(frame, line)
                else:
                    node = AstNode(NodeType.blockmath if line.startswith("$$") else NodeType.blockcode, frame)
                    frame.kids.append(node)
                    frame = node
                continue
            elif stat.math or stat.code:
                puttext(frame, line)
                continue

            self.parseLine(line, frame)
            node = frame.kids[-1]

            if node.type == NodeType.row:
                if not stat.table:
                    frame.kids.pop()
                    table = AstNode(NodeType.table, frame)
                    frame.kids.append(table)
                    frame = table
                    table.kids.append(node)
                    node.parent = table
                    stat.table = True
                else:
                    frame.kids.append(node)
            elif stat.table:
                kids = []
                while frame.kids[-1].type != NodeType.row:
                    kids.append(frame.kids[-1])
                    kids[-1].parent = frame.parent
                    frame.kids.pop()
                frame = frame.parent
                frame.kids.extend(kids)
                stat.table = False

            item = node.type == NodeType.listitem or node.type == NodeType.ordereditem
            if not item:
                while stat.lev > 0:
                    stat.lastspace.pop()
                    stat.lev -= 1
                continue

            while stat.lev > 0 and node.conf.get("prespace", 0) < stat.lastspace[stat.lev]:
                stat.lev -= 1
                stat.lastspace.pop()

            if node.conf.get("prespace", 0) > stat.lastspace[stat.lev]:
                stat.lastspace.append(node.conf.get("prespace", 0))
                stat.lev += 1

            node.conf["level"] = stat.lev
            maxlev = max(maxlev, stat.lev + 1)

        kids = []
        stat.node, stat.last = frame, NodeType.empty

        def putkid(node, nxt):
            if node is frame:
                print("down at root")
                kids.append(nxt)
            else:
                node.kids.append(nxt)

        def downkid(node, nt=NodeType.text):
            nxt = AstNode(nt, node)
            putkid(node, nxt)
            return nxt

        frame.conf["level"] = -1
        for kid in frame.kids:
            if kid.type != stat.last:
                stat.node = frame

            if "level" in kid.conf and kid.type == NodeType.listitem or kid.type == NodeType.ordereditem:
                while kid.conf["level"] < stat.node.conf["level"]:
                    stat.node = stat.node.parent

                if stat.node is frame or kid.conf["level"] > stat.node.conf["level"]:
                    stat.node = downkid(stat.node, NodeType.list if kid.type == NodeType.listitem else NodeType.ordered)
                    stat.node.conf["level"] = kid.conf["level"]
                    stat.node.kids.append(kid)
                elif kid.conf["level"] == stat.node.conf["level"]:
                    stat.node.kids.append(kid)

                stat.last = kid.type
            else:
                stat.node, stat.last = frame, NodeType.empty
                kids.append(kid)
        frame.kids = kids

    def parseLine(self, line, master):
        print("Parsing Line: ", line)
        stat, node = Stat(), AstNode(NodeType.text, master)
        master.kids.append(node)

        stat.math, stat.code = False, False
        stat.weight, stat.sout = 0, False

        stat.level = 0
        stat.table, stat.col = False, 0
        stat.prespace, stat.item = 0, False

        def downkid(node, nt=NodeType.text):
            nxt = AstNode(nt, node)
            node.kids.append(nxt)
            return nxt

        def brokid(node, nt=NodeType.text):
            nxt = AstNode(nt, node.parent)
            node.parent.kids.append(nxt)
            return nxt

        # check pause
        if line.startswith("<v-click>"):
            node.type = NodeType.pause
            node.conf["pause-type"] = 1
        elif line.startswith("<v-clicks>"):
            node.type = NodeType.pause
            node.conf["pause-type"] = 2
        elif line.startswith("</v-click>"):
            node.type = NodeType.pause
            node.conf["pause-type"] = -1
        elif line.startswith("</v-clicks>"):
            node.type = NodeType.pause
            node.conf["pause-type"] = -2
        if node.type == NodeType.pause:
            return node

        # heading
        if line.startswith("#"):
            for c in line:
                if c == '#':
                    stat.level += 1
                else:
                    break
            node.type = NodeType.heading
            node.conf["level"] = stat.level
            title = AstNode(NodeType.empty)
            self.parseLine(line.strip('# '), title)
            node.conf["title"] = title
            return node

        # quote
        if line.startswith("> "):
            node.type = NodeType.quote
            node, line = downkid(node), line[2:]


        # count white space !
        # 在之后一定是可以层级叠叠叠的，也就是可能放在列表缩进里面
        for c in line:
            if c == ' ':
                stat.prespace += 1
            elif c == '\t':
                stat.prespace += TABSIZE
            else:
                break
        node.conf["prespace"] = stat.prespace
        line = line.lstrip()

        # check table
        if line.startswith("|"):
            parts = line.split("|")[1:-1] # remove white space outside.
            node.type = NodeType.row
            node.conf["cols"] = len(parts)
            for part in parts:
                partnode = AstNode(NodeType.empty, node)
                node.kids.append(partnode)
                self.parseLine(part.strip(), partnode)
            return node

        # check list
        if matdict["list"].match(line):
            node.type = NodeType.listitem
            node, line = downkid(node), line[2:]
        elif matdict["ordered"].match(line):
            node.type = NodeType.ordereditem
            number, text = line.split('.', 1)
            node.conf["item-index"] = int(number)
            node, line = downkid(node), text[1:]

        vis, starcnt = [False] * len(line), 0
        for i, c in enumerate(line):
            if vis[i]:
                continue
            # 处理转译字符，需要 incode 或者 inmath 的时候
            if (stat.code or stat.math) and c == '\\' and i + 1 < len(line):
                vis[i + 1] = True
                node.text += c + line[i + 1]
                continue

            if stat.code:
                if c == '`':
                    stat.code = False
                    node = brokid(node)
                else:
                    node.text += c
                continue
            elif not stat.math and c == '`':
                node = brokid(node, NodeType.incode)
                stat.code = True
                continue

            if stat.math:
                if c == '$':
                    stat.math = False
                    node = brokid(node)
                else:
                    node.text += c
                continue
            elif c == '$':
                node = brokid(node, NodeType.inmath)
                stat.math = True
                continue

            if c == '!':
                mat = matdict["link"].match(line[i + 1:])
                if mat:
                    img = AstNode(NodeType.img, node.parent)
                    img.conf["src"], img.conf["alt"] = mat.group(2), mat.group(1)
                    node.parent.kids.append(img)
                    node = brokid(node)
                    for k in range(mat.end()):
                        vis[i + k] = True
                    continue

            if c == '[':
                mat = matdict["link"].match(line[i:])
                if mat:
                    link = AstNode(NodeType.link, node.parent)
                    link.conf["src"], link.conf["alt"] = mat.group(2), mat.group(1)
                    node.parent.kids.append(link)
                    node = brokid(node)
                    for k in range(mat.end()):
                        vis[i + k] = True
                    continue

            # BUG: 目前认为不存在 bold 和 italic 间隔嵌套的情况
            # 也就是无法处理 ***Italic* Bold** 这种神秘的东西
            if c == '*':
                starcnt = 0
                for j in range(i, len(line)):
                    if line[j] == '*':
                        starcnt += 1
                        vis[j] = True
                    else:
                        break

                if stat.weight == 0:
                    if starcnt == 1:
                        node = brokid(node, NodeType.italic)
                    elif starcnt == 2:
                        node = brokid(node, NodeType.bold)
                    elif starcnt == 3:
                        node = brokid(node, NodeType.italic)
                        node = downkid(node, NodeType.bold)
                    else:
                        raise "**** TOOOO MUCH!!!"
                    stat.weight = starcnt
                else:
                    assert stat.weight == starcnt, "line " + line + " * count not match !!!!"
                    if starcnt == 3:
                        node = node.parent
                    node = brokid(node)
                continue
            else:
                starcnt = 0

            node.text += c
