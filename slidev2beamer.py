# #!/usr/bin/env python
from scanner import Scanner
from tokenizer import Tokenizer
from renderer import Renderer

scan = Scanner("docs/share-ds")
root = scan.parseFile("slides.md")
tokenizer = Tokenizer()
tokenizer.analyze(root)

print("Tokenized")
root.output()

renderer = Renderer()
renderer.render(root)
renderer.output(root)
